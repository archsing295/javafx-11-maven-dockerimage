JavaFX-11-maven Docker image
============================

Contains:
 * OpenJDK 11
 * maven 3
 * dependencies necessary for Monocle

Usage
-----

Just use as the (base) image to run your CI jobs in.

An example test case and necessary settings for `pom.xml` can be found in this repository.
